//use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
const app = express();

// //set dynamic views file
// app.set('views',path.join(__dirname,'views'));

//set views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');

//set public folder as static folder for static file
app.use(express.static('public'));
//route untuk halaman home
app.get('/',(req, res) => {
    //render file index.hbs
    res.render('index', {
        name : "Komang"
    });
});

app.get('/post', (req, res) =>{
    res.end(`
        <!Doctype html>
        <html>
            <body>
                <h1>Tambah User Baru</h1>
                <input id="input" value="Name"/>
                <input type='button' id="btn_submit" onclick='klikk()' value='submit' />
                <script>
                    function klikk() {
                        let inputNama = document.getElementById('input').value;
                        res.render('index', {
                            name : inputNama
                        });
                    }
                </script>
            </body>
        </html>
    `);
});

// route untuk halaman home dengan parameter name
app.get('/:name',(req, res) => {
    //render file index.hbs
    res.render('index',{
        name : req.params.name
    });
});

//route untuk halaman about
// app.get('/about',(req, res) => {
//     res.send('This is about page');
// });

app.listen(8000, () => {
    console.log('Server is running at port 8000');
});